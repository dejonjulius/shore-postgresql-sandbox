# Shore.Umbrella

docker-compose up -d

`(cd apps/shore && mix ecto.setup)` - This will generate 1000 users

`docker exec -it shore-postgresql-sandbox bash`

`psql -U shore shore_dev` - Access the database

`\timing` - On the timing

`SELECT \* FROM users;`

---


Output retrieving 1000 data
Time: 11.699 ms

This does Full text search in one column (first_name)

`iex -S mix`
`alias Shore.Accounts.User`
`alias Shore.Repo`
`User |> User.search("john") |> Repo.all`

```
Interactive Elixir (1.9.0) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)> alias Shore.Accounts.User
Shore.Accounts.User
iex(2)> alias Shore.Repo
Shore.Repo
iex(3)> User |> User.search("mike") |> Repo.all
[debug] QUERY OK source="users" db=2.6ms decode=1.3ms queue=0.7ms
SELECT u0."id", u0."address_1", u0."city", u0."company", u0."email", u0."first_name", u0."last_name", u0."state", u0."zipcode", u0."inserted_at", u0."updated_at" FROM "users" AS u0 WHERE (u0."first_name" % $1) ORDER BY similarity(u0."first_name", $2) DESC ["mike", "mike"]
[]
iex(4)> User |> User.search("mil") |> Repo.all
[debug] QUERY OK source="users" db=19.1ms
SELECT u0."id", u0."address_1", u0."city", u0."company", u0."email", u0."first_name", u0."last_name", u0."state", u0."zipcode", u0."inserted_at", u0."updated_at" FROM "users" AS u0 WHERE (u0."first_name" % $1) ORDER BY similarity(u0."first_name", $2) DESC ["mil", "mil"]
[
  %Shore.Accounts.User{
    __meta__: #Ecto.Schema.Metadata<:loaded, "users">,
    address_1: "3 Keeling Fork",
    city: "New Chase",
    company: "Bernier, Ratke and Klocko",
    email: "barton.muller@crona.com",
    first_name: "Milan",
    id: 760,
    inserted_at: ~N[2019-07-17 12:36:09],
    last_name: "Emard",
    state: "NC",
    updated_at: ~N[2019-07-17 12:36:09],
    zipcode: "96355"
  },
  %Shore.Accounts.User{
    __meta__: #Ecto.Schema.Metadata<:loaded, "users">,
    address_1: "9 Emmie Pass",
    city: "New Franco",
    company: "Daugherty Inc",
    email: "khalil2043@heathcote.name",
    first_name: "Millie",
    id: 321,
    inserted_at: ~N[2019-07-17 12:35:54],
    last_name: "Cremin",
    state: "FL",
    updated_at: ~N[2019-07-17 12:35:54],
    zipcode: "58895"
  }
]
```

see query_test.exs for Postgres Full text Search tests
eg.

```
    test "Can search pluralized words " do
      query_result =
        Ecto.Adapters.SQL.query!(
          Repo,
          # Given The quick brown fox jumped over the lazy dog
          "SELECT to_tsvector('The quick brown fox jumped over the lazy dog')
          @@ to_tsquery('foxes');"
          # When we search for `foxes`
        )

      # Then return
      assert query_result.rows == [[true]]
    end
```
