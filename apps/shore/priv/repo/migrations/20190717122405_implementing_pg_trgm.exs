defmodule Shore.Repo.Migrations.ImplementingPgTrgm do
  use Ecto.Migration

  def up do
    # Creates Extension for pg_trgm
    execute "CREATE extension if not exists pg_trgm;"
    # Indexing data for Text search integration
    # https://www.postgresql.org/docs/9.4/pgtrgm.html
    execute "CREATE INDEX users_first_name_trgm_index ON users USING gin (first_name gin_trgm_ops);"
    execute "CREATE INDEX users_last_name_trgm_index ON users USING gin (last_name gin_trgm_ops);"
    execute "CREATE INDEX users_email_trgm_index ON users USING gin (email gin_trgm_ops);"
    execute "CREATE INDEX users_address_1_trgm_index ON users USING gin (address_1 gin_trgm_ops);"
    execute "CREATE INDEX users_city_trgm_index ON users USING gin (city gin_trgm_ops);"
    execute "CREATE INDEX users_company_trgm_index ON users USING gin (company gin_trgm_ops);"
    execute "CREATE INDEX users_state_trgm_index ON users USING gin (state gin_trgm_ops);"
    execute "CREATE INDEX users_zipcode_trgm_index ON users USING gin (zipcode gin_trgm_ops);"
  end

  def down do
    execute "DROP INDEX users_first_name_trgm_index;"
    execute "DROP INDEX users_last_name_trgm_index;"
    execute "DROP INDEX users_email_trgm_index;"
    execute "DROP INDEX users_address_1_trgm_index;"
    execute "DROP INDEX users_city_trgm_index;"
    execute "DROP INDEX users_company_trgm_index;"
    execute "DROP INDEX users_state_trgm_index;"
    execute "DROP INDEX users_zipcode_trgm_index;"
  end
end
