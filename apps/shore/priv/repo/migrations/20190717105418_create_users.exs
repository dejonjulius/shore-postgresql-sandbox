defmodule Shore.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :first_name, :string
      add :last_name, :string
      add :company, :string
      add :email, :string
      add :address_1, :string
      add :city, :string
      add :state, :string
      add :zipcode, :string

      timestamps()
    end
  end
end
