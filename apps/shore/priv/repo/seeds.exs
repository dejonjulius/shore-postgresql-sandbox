# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Shore.Repo.insert!(%Shore.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Shore.Accounts

Enum.each(0..1000, fn x ->
  user_attrs = %{
    first_name: Faker.Name.first_name(),
    last_name: Faker.Name.last_name(),
    company: Faker.Company.name(),
    email: Faker.Internet.email(),
    address_1: Faker.Address.street_address(),
    city: Faker.Address.city(),
    state: Faker.Address.state_abbr(),
    zipcode: Faker.Address.zip_code()
  }

  Accounts.create_user(user_attrs)
end)
