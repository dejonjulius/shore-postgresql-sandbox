defmodule Shore.Accounts.User do
  @moduledoc """
  User
  """
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  schema "users" do
    field :address_1, :string
    field :city, :string
    field :company, :string
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :state, :string
    field :zipcode, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [
      :first_name,
      :last_name,
      :company,
      :email,
      :address_1,
      :city,
      :state,
      :zipcode
    ])
    |> validate_required([
      :first_name,
      :last_name,
      :company,
      :email,
      :address_1,
      :city,
      :state,
      :zipcode
    ])
  end

  def search(query, search_term) do
    from(u in query,
      where: fragment("? % ?", u.first_name, ^search_term),
      order_by: fragment("similarity(?, ?) DESC", u.first_name, ^search_term)
    )
  end
end
