defmodule Shore.Repo do
  use Ecto.Repo,
    otp_app: :shore,
    adapter: Ecto.Adapters.Postgres
end
