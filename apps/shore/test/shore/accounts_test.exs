defmodule Shore.AccountsTest do
  use Shore.DataCase

  alias Shore.Accounts

  describe "users" do
    alias Shore.Accounts.User

    @valid_attrs %{
      address_1: "some address_1",
      city: "some city",
      company: "some company",
      email: "some email",
      first_name: "some first_name",
      last_name: "some last_name",
      state: "some state",
      zipcode: "42"
    }
    @update_attrs %{
      address_1: "some updated address_1",
      city: "some updated city",
      company: "some updated company",
      email: "some updated email",
      first_name: "some updated first_name",
      last_name: "some updated last_name",
      state: "some updated state",
      zipcode: "43"
    }
    @invalid_attrs %{
      address_1: nil,
      city: nil,
      company: nil,
      email: nil,
      first_name: nil,
      last_name: nil,
      state: nil,
      zipcode: nil
    }

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.address_1 == "some address_1"
      assert user.city == "some city"
      assert user.company == "some company"
      assert user.email == "some email"
      assert user.first_name == "some first_name"
      assert user.last_name == "some last_name"
      assert user.state == "some state"
      assert user.zipcode == "42"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Accounts.update_user(user, @update_attrs)
      assert user.address_1 == "some updated address_1"
      assert user.city == "some updated city"
      assert user.company == "some updated company"
      assert user.email == "some updated email"
      assert user.first_name == "some updated first_name"
      assert user.last_name == "some updated last_name"
      assert user.state == "some updated state"
      assert user.zipcode == "43"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end
end
