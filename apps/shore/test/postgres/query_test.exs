defmodule Shore.QueryTest do
  alias Shore.Repo
  alias Shore.Accounts

  use ExUnit.Case, async: true

  setup do
    # Explicitly get a connection before each test
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Repo)
  end

  describe "Full Text Search" do
    test "Joins 3 Columns using the AS Document" do
      user_attrs = %{
        first_name: "Julius",
        last_name: "Dejon",
        company: "Shore Suite",
        email: "julius.dejon@shoresuite.com",
        address_1: "21 Road 12 Project 8 Barangay Bahay Toro",
        city: "Quezon City",
        state: "QZC",
        zipcode: "1106"
      }

      Accounts.create_user(user_attrs)

      query_result =
        Ecto.Adapters.SQL.query!(
          Repo,
          "SELECT first_name || ' ' || last_name || ' ' || address_1 AS document FROM users"
        )

      assert query_result.rows == [["Julius Dejon 21 Road 12 Project 8 Barangay Bahay Toro"]]
    end

    test "Can search pluralized words " do
      query_result =
        Ecto.Adapters.SQL.query!(
          Repo,
          # Given The quick brown fox jumped over the lazy dog
          "SELECT to_tsvector('The quick brown fox jumped over the lazy dog')
          @@ to_tsquery('foxes');"
          # When we search for `foxes`
        )

      # Then return
      assert query_result.rows == [[true]]
    end

    test "Can search related words " do
      query_result =
        Ecto.Adapters.SQL.query!(
          Repo,
          # Given The quick brown fox jumped over the lazy dog
          "SELECT to_tsvector('The quick brown fox jumped over the lazy dog')
          @@ to_tsquery('jumping');"
          # When we search for jumping
        )

      # Then return
      assert query_result.rows == [[true]]
    end
  end
end
